@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-5">
            <img src="{{$user->profile->profileImage()}}" class="rounded-circle w-100">
        </div>
        <div class="col-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <div class="d-flex align-items-center mb-4">
                    <h1 class="me-3">{{$user->username}}</h1>
                    <div id="follow-button" data-user="{{$user->id}}" data-follows="{{$follows}}"></div>
                </div>
                @can('update', $user->profile)
                    <a href="/p/create">Add New Post</a>
                @endcan
                
            </div>

            @can('update', $user->profile)
                <a href="/profile/{{ $user->id }}/edit">Edit profile</a>
            @endcan
            
            <div class="d-flex">
                <div class="pe-5"><strong>{{$postCount}}</strong> posts</div>
                <div class="pe-5"><strong>{{$followers}}</strong> followers</div>
                <div class="pe-5"><strong>{{$following}}</strong> following</div>
            </div>
            <div class="pt-4 fw-bold">{{$user->profile->title ?? 'N/A'}}</div>
            <p>{{$user->profile->description ?? 'N/A' }}</p>
            <div><a href="#">{{$user->profile->url ?? 'N/A'}}</a></div>
        </div>
    </div>

    <div class="row pt-5">
        @foreach($user->posts as $post)
        <div class="col-4 pb-4">
            <a href="/p/{{$post->id}}">
                <img src="/storage/{{$post->image}}" class="w-100" alt="{{$post->caption}}">
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection