@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/profile/{{$user->id}}" enctype="multipart/form-data" method="post">
        @csrf
        @method('PATCH')

        <div class="row">
            <div class="col-8 offset-2">
                <div class="form-group row">
                    <label for="title" class="col-form-label">Title</label>
                    <input id="title" type="text" 
                            class="form-control @error('title') is-invalid @enderror"
                            name="title"
                            value="{{ old('title') ?? $user->profile->title }}" 
                            required autocomplete="name" autofocus>

                    @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label for="description" class="col-form-label">Description</label>
                    <input id="description"
                            type="text"
                            class="form-control @error('description') is-invalid @enderror"
                            name="description"
                            value="{{ old('description') ?? $user->profile->description }}" 
                            required autocomplete="name" autofocus>

                    @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group row">
                    <label for="url" class="col-form-label">Url</label>
                    <input id="url"
                            type="text"
                            class="form-control @error('url') is-invalid @enderror" 
                            name="url" 
                            value="{{ old('url') ?? $user->profile->url }}" 
                            autocomplete="name" autofocus>

                    @error('url')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="row">
                    <label for="image" class="col-form-label">Profile picture</label>
                    <input type="file" name="image" id="image" class="form-control-file">

                    @error('image')
                        <strong>{{ $message }}</strong>
                    @enderror

                </div>
                <div class="row pt-4"><button class="btn btn-primary">Save Profile</button></div>
            </div>
        </div>
    </form>
</div>
@endsection