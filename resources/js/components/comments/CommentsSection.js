import { hasData } from 'jquery'
import React from 'react'
import ReactDOM from 'react-dom'
import Comments from './Comments'

const $commentsSection = document.getElementById('comments-section')

if ($commentsSection) {
    const data = JSON.parse($commentsSection.dataset.comments).map((comment) => ({
        username: comment.user.username,
        content: comment.content,
        date: comment.created_at,
        userId: comment.user_id
    }))

    const isAuth = !!$commentsSection.dataset.auth
    const postId = Number($commentsSection.dataset.postid)

    class CommentSection extends React.Component {
        state = {
            comments: this.props.comments
        }

        handleAddComment = async (content) => {
            try {
                const response = await axios.post('/comments', { content, post_id: this.props.postId })

                const data = response.data
                this.setState((prevState) => {
                    prevState.comments.splice(0, 0, { username: data.user.username, content: data.content, date: data.created_at, userId: data.user_id })
                    return { comments: prevState.comments }
                })
            } catch (error) {
                console.error(error)
            }

        }

        render = () => (
            <Comments comments={this.state.comments} isAuth={isAuth} handleAddComment={this.handleAddComment} />
        )
    }

    ReactDOM.render(< CommentSection comments={data} postId={postId} />, $commentsSection)
}